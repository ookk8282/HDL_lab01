// behavior modeling
module lab1_3(a, b, c, aluctr, d, e);
input a, b, c;
input [1:0] aluctr;
output d, e;
reg d, e;

always@(a or b or c or aluctr) begin
    case (aluctr)
        2'b00   : d = a ^ b ^ c;
        2'b01   : d = a & b;
        2'b10   : d = a | b;
        default : d = a ^ b;
    endcase
    e = |aluctr ? 1'b0 : (a & b) | (a & c) | (b & c);
end
endmodule
