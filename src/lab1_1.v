// structural modeling(using gate primitives)
module lab1_1(a, b, c, aluctr, d, e);
input a, b, c;
input [1:0] aluctr;
output d, e;
wire m0, m1, m2, m3;
wire addition;
wire e_;

mux4_to_1 mux(d, m0, m1, m2, m3, aluctr);
xor(m0, a, b, c);
and(m1, a, b);
or(m2, a, b);
xor(m3, a, b);

// e = 1 if $addition and majority3(a,b,c)
and(e, addition, e_);
nor(addition, aluctr[0], aluctr[1]);
majority3 maj(e_, a, b, c);
endmodule


module majority3(z, a, b, c);
input a, b, c;
output z;
wire ab, ac, bc;

and(ab, a, b);
and(ac, a, c);
and(bc, b, c);
or(z, ab, ac, bc);
endmodule
