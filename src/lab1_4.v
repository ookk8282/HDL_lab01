module lab1_4(a, b, c, aluctr, d, e);
input [3:0] a, b;
input c;
input [1:0] aluctr;
output [3:0] d;
output e;

wire [3:0] cin;
assign cin[0] = c;

lab1_1 alu0(a[0], b[0], cin[0], aluctr, d[0], cin[1]);
lab1_1 alu1(a[1], b[1], cin[1], aluctr, d[1], cin[2]);
lab1_1 alu2(a[2], b[2], cin[2], aluctr, d[2], cin[3]);
lab1_1 alu3(a[3], b[3], cin[3], aluctr, d[3],      e);

endmodule
