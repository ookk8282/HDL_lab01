// flow modeling (using continuous assignment)
module lab1_2(a, b, c, aluctr, d, e);
input a, b, c;
input [1:0] aluctr;
output d, e;

assign d = aluctr == 2'b00    ? a ^ b ^ c
         : aluctr == 2'b01    ? a & b
         : aluctr == 2'b10    ? a | b
                              : a ^ b;
assign e = |aluctr ? 1'b0 : (a & b) | (a & c) | (b & c);

endmodule
