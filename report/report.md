# 硬體設計與實驗 - lab1 - report

#### 104062124 - 張嘉軒

## 描述
lab1的題目是實作出 1-bit 和 4-bit 的 ALU ，ALU 的功能包括
1. addition
2. and
3. or
4. xor

1-bit 的 ALU，要使用三種不同的寫法，包含
1. Structural Modeling
2. Data Flow Modeling
3. Behavioral Modeling

## 實作過程
### lab1_1
在寫 Structural Modeling 的時候，還可以使用助教提供的 `mux_4_to_1`，在把電路圖畫出來之後，就只要再使用 Verilog 的語法把它描述出來就完成了。

值得一提的是，我將 **MAJ(majority)** 抽離出來，寫成一個獨立的 module 增加了程式碼的可讀性；另外我在 e 的條件判斷上，運用 `addition = xor(aluctr)`以及一個 and gate，使得`e`在 ALU 的功能不是加法時，值都會是 0，否則 `e = xor(a, b, c)`。

![1-bit_ALU](https://gitlab.com/ookk8282/HDL_lab01/raw/dccccb0616098a0b894505c404b0c0535d7c95ab/report/1-bit_ALU.jpg "1-bit ALU")





![majority](https://gitlab.com/ookk8282/HDL_lab01/raw/dccccb0616098a0b894505c404b0c0535d7c95ab/report/majority.jpg "majority")

### lab1\_2 lab1\_3
而在 Data Flow Modeling 及 Behavior Modeling 的寫法上，就可以使用一些 Verilog 便利的語法，來做到 **MUX** 的效果。

```Verilog
// Data Flow Modeling 
assign d = aluctr == 2'b00 ? a ^ b ^ c          
         : aluctr == 2'b01 ? a & b          
         : aluctr == 2'b10 ? a | b
                           : a ^ b;
assign e = |aluctr ? 1'b0 : (a & b) | (a & c) | (b & c);
```

```Verilog
// Behavior Modeling
always@(a or b or c or aluctr) begin
	case (aluctr)         
    	2'b00   : d = a ^ b ^ c;
        2'b01   : d = a & b;
        2'b10   : d = a | b;
        default : d = a ^ b;     
    endcase     
    e = |aluctr ? 1'b0 : (a & b) | (a & c) | (b & c); 
end
```

### lab1_4
完成了 1-bit 之後，就可以開始設計如何 **reuse** 1-bit 來實作出 4-bit 的 ALU。

因為加法的行為跟其他三個功能的行為比較不一樣，因此在設計時，我是將他們分開來畫圖。

把功能為加法的 ALU 直接當成 Full Adder 來設計；而 and, or, xor 就只是單純的 bitwise。

![4-bit FA](https://gitlab.com/ookk8282/HDL_lab01/raw/dccccb0616098a0b894505c404b0c0535d7c95ab/report/4-bit_FA.jpg "4-bit FA")



![bitwise](https://gitlab.com/ookk8282/HDL_lab01/raw/dccccb0616098a0b894505c404b0c0535d7c95ab/report/bitwise_function.jpg "bitwise function")

在這兩張圖中，我省略了一些 port ，沒有畫出來，是因為它們並不影響主要功能的表達。

如：`aluctr`就只是直接接在每一個`1-bit ALU`上；bitwise 的圖中，input-`d`的值並不會影響到 output 的值，output-`e`則會固定為0。

## 補充
在寫程式碼的過程中，我嘗試使用 Git 來管理我的程式碼，並且上傳到網路上(權限在一開始是設成 **private**，在 code 繳交日期過後，才轉成公開的，所以沒有讓別人抄襲的問題)。

https://gitlab.com/ookk8282/HDL_lab01